const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = ['requesttype','totalprice'];

const attToGet = ['cart_id', 'totalprice'];
const attToQuery = ['cart_id','totalprice','cart_items'];

const cartSchema =  {
    hashKey: 'cart_id',
    rangeKey:'user_id',
    //hashKey: 'user_id',
    timestamps: true,
    schema: Joi.object({
        cart_id  : Joi.string(),
        user_id   : Joi.string(),
        cart_items   : Joi.object(),
        totalquantity : Joi.number().positive(),
        totalprice : Joi.number().positive(),
        totalplans : Joi.number().positive(),
        totalweight: Joi.number().positive(),
        requesttype : Joi.string(),
        subscription_config : Joi.object()
    }).unknown(true).optionalKeys(optionalParams)
};

const optionsObj = {
    attToGet,
    attToQuery,
};
const Cart = SchemaModel(cartSchema,optionsObj); // create model by passing the table name of the store

module.exports = Cart;